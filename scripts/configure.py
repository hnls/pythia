#!/usr/bin/env python

# configure.py is a part of the PYTHIA event generator.
# Copyright (C) 2021 Philip Ilten, Torbjorn Sjostrand.
# PYTHIA is licenced under the GNU GPL v2 or later, see COPYING for details.
# Please respect the MCnet Guidelines, see GUIDELINES for details.

# This program calculates both the relevant HNL decays and decays
# producing an HNL from heavy meson decays. This program can be used
# either as a module or script. For script usage help, run
# "./configure.py -h". A configuration file that can be read by
# "Pythia::readFile" is produced.

#===========================================================================
def decays(pdb, lep, mass, us, decs = None):
    """
    Returns an ordered dictionary of relevant HNL decays and decays
    producing HNLs where each key is the decaying particle and the
    value is a list of decays of the form: (on mode, branching ratio,
    matrix element, [dtrs]).

    pdb:  populated ParticleDatabase object (Pythia.particleDatabase).
    lep:  charged lepton PID.
    mass: mass of the HNL in GeV.
    us:    lepton mixing angles, [ue, umu, utau].
    decs: optionally, update an existing decay dictionary.
    """
    import collections
    if us[int((lep - 11)/2)] == 0: return
    if decs == None: decs = collections.OrderedDict()
    pdb.m0(18, mass)
    decs[0] = ["18:isResonance = false", "18:m0 = %f" % mass]
    
    # Function to check phase space.
    ps = lambda mom, dtrs, pdb = pdb: pdb.m0(mom) > sum(
        [pdb.m0(dtr) for dtr in dtrs])
    
    # Charged current HNL decays (swap particles and anti-particles).
    mom = 18
    pde = pdb.findParticle(15)
    if not mom in decs: decs[mom] = []
    for chn in range(0, pde.sizeChannels()):
        chn = pde.channel(chn)
        on, br, me = chn.onMode(), 0, chn.meMode()
        dtrs = [chn.product(dtr) for dtr in range(0, chn.multiplicity())]
        dtrs[0] = lep
        for idx, dtr in enumerate(dtrs[1:]):
            if pdb.hasAnti(dtr): dtrs[idx + 1] = -dtr
        if me > 1500 and ps(mom, dtrs):
            decs[mom] += [(on, br, me, tuple(dtrs))]

    # Neutral current HNL decays.
    on, br, me, dtrs = 1, 0, 1521, [lep + 1, 111]
    if ps(mom, dtrs): decs[mom] += [(on, br, me, tuple(dtrs))]
    on, br, me, dtrs = 1, 0, 1532, [lep + 1, 211, -211]
    if ps(mom, dtrs): decs[mom] += [(on, br, me, tuple(dtrs))]
    on, br, me, dtrs = 1, 0, 1541, [lep + 1, 211, -211, 111]
    if ps(mom, dtrs): decs[mom] += [(on, br, me, tuple(dtrs))]
    
    # Production from heavy mesons (uses arXiv:2103.11494v1).
    for mom in [411, 421, 431, 511, 521, 531, 541]:
        pde = pdb.findParticle(mom)
        if not mom in decs: decs[mom] = []
    
        # Fully leptonic.
        if pde.chargeType() != 0:
            on, br, me, dtrs = 1, 0, 0, [18, -lep]
            if ps(mom, dtrs): decs[mom] += [(on, br, me, tuple(dtrs))]
    
        # Semi leptonic.
        for chn in range(0, pde.sizeChannels()):
            chn = pde.channel(chn)
            on, br, me = chn.onMode(), 0, chn.meMode()
            dtrs = [chn.product(dtr) for dtr in range(0, chn.multiplicity())]
            if len(dtrs) != 3: continue
            if dtrs[0:2] == [-11, 12] or dtrs[0:2] == [12, -11]:
                spin = pdb.spinType(dtrs[2])
                if spin == 1: dtrs[0], dtrs[1] = 18, -lep  # Pseudo-scalar.
                else: dtrs[0], dtrs[1] = -lep, 18          # Vector.
                if ps(mom, dtrs): decs[mom] += [(on, br, me, tuple(dtrs))]

    # Production from resonances.
    for mom in [23, 24, 25, 32]:
        pde = pdb.findParticle(mom)
        if not mom in decs: decs[mom] = []
        for chn in range(0, pde.sizeChannels()):
            chn = pde.channel(chn)
            on, br, me = chn.onMode(), 0, chn.meMode()
            dtrs = [chn.product(dtr) for dtr in range(0, chn.multiplicity())]
            if len(dtrs) != 2: continue

            # Neutral.
            if dtrs[0:2] == [lep, -lep]:
                dtrs[0], dtrs[1] = -18, lep + 1
                if ps(mom, dtrs): decs[mom] += [(on, br, me, tuple(dtrs))]
                dtrs[0], dtrs[1] = 18, -(lep + 1)
                if ps(mom, dtrs): decs[mom] += [(on, br, me, tuple(dtrs))]

            # Charged.
            elif dtrs[0:2] == [-lep, lep + 1]:
                dtrs[0:2] = [-lep, 18]
                if ps(mom, dtrs): decs[mom] += [(on, br, me, tuple(dtrs))]
                
    # Return the output.
    return decs

#===========================================================================
def configure(pdb, decs, out = None):
    """
    Convert a decay dictionary into a list of readable configuraiton
    strings by Pythia 8. Return the configuration string list. If
    "out" is specified, write the configuration to file as well.

    pdb:  populated ParticleDatabase object (Pythia.particleDatabase).
    dec:  decay list (on mode, branching ratio,  matrix element, [dtrs]).
    out:  configuration output filename.
    """
    cfgs = []
    sep  = "#" + "="*74 + "\n"
    if out:
        out = open(out, "w")
        for cfg in cfgs: out.write(cfg + "\n")
    for mom, dec in decs.items():
        if mom == 0:
            cfgs += dec
            if out:
                for cfg in dec: out.write(cfg + "\n")
            continue
        elif out: out.write(sep + (
                "# %s decay channels.\n" % pdb.name(mom)) + sep)
        for on, br, me, dtrs in dec:
            cfgs += ["%i:addChannel = %i %.7f %i %s" % (
                mom, on, br, me, " ".join([str(dtr) for dtr in dtrs]))]
            if out:
                out.write("# " + " ".join([pdb.name(dtr) for dtr in dtrs])
                          + "\n" + cfgs[-1] + "\n")
    if out: out.close()
    return cfgs
    
#===========================================================================
if __name__ == "__main__":
    """
    Main script. Produces a Pythia 8 configuration file.
    """
    # Parse arguments.
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument(
        "--out", default = "hnls.cfg",
        help = "Output filename.", type = str)
    parser.add_argument(
        "--cfg", default = "",
        help = "Optional Pythia 8 configuration file.", type = str)
    parser.add_argument(
        "--mass", default = 0,
        help = "HNL mass in GeV.", type = float)
    parser.add_argument(
        "--ue", default = 0,
        help = "Electron mixing angle.", type = float)
    parser.add_argument(
        "--umu", default = 0,
        help = "Muon mixing angle.", type = float)
    parser.add_argument(
        "--utau", default = 0,
        help = "Tau mixing angle.", type = float)
    args = parser.parse_args()
    
    # Configure Pythia.
    import pythia8, collections
    py  = pythia8.Pythia("", False)
    pdb = py.particleData
    if args.cfg: py.readFile(args.cfg)

    # Calculate the decays and write output.
    decs = collections.OrderedDict()
    for lep in [11, 13, 15]:
        decays(pdb, lep, args.mass, [args.ue, args.umu, args.utau], decs)
    configure(pdb, decs, args.out)
