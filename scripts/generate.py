#!/usr/bin/env python

# generate.py is a part of the PYTHIA event generator.
# Copyright (C) 2021 Philip Ilten, Torbjorn Sjostrand.
# PYTHIA is licenced under the GNU GPL v2 or later, see COPYING for details.
# Please respect the MCnet Guidelines, see GUIDELINES for details.

# This program generates example HNL production and decay and can be
# used either as a module or script. For script usage help, run
# "./generate.py -h".

#===========================================================================
class Generator:
    """
    Class to generate a specific production and decay of an HNL.

    py:    local Pythia instance.
    mom:   PID of the mother particle.
    init:  flag if generator is correctly initialized.
    event: event record for the current event.
    pros:  dictionary of available production channels.
    decs:  dictionary of available production channels.
    """
    def __init__(self, com, mom, mass, us, pro = -1, dec = -1,
                 pre = None, post = None):
        """
        Initialize the generator.

        com:  COM in GeV if vector mother, else mother energy.
        mom:  PID of the HNL mother.
        mass: mass of the HNL in GeV.
        us:   lepton mixing angles, [ue, umu, utau].
        pro:  production channel index; if unknown, < 0 returns.
        dec:  decay channel index; if unknown, < 0 returns.
        """
        import pythia8, configure, collections
        self.py  = pythia8.Pythia("", False)
        self.com = com
        self.mom  = mom
        self.init = False
        
        # Apply user pre-configuration.
        if type(pre) == str: self.py.readFile(pre)
        elif pre:
            for cfg in pre: self.py.readString(cfg)

        # Process configuration, otherwise particle gun.
        cfgs = ["Beams:eCM = %f" % com, "PartonLevel:MPI = off",
                "PartonLevel:ISR = off", "PartonLevel:FSR = off",
                "HadronLevel:Hadronize = off",
                "Init:showChangedParticleData = off"]
        self._next = lambda gen: gen.py.next()
        if   mom == 22: cfgs += [
                "WeakSingleBoson:ffbar2gmZ = on",
                "WeakZ0:gmZmode = 1"]
        elif mom == 23: cfgs += [
                "WeakSingleBoson:ffbar2gmZ = on",
                "WeakZ0:gmZmode = 2"]
        elif mom == 32: cfgs += [
                "NewGaugeBoson:ffbar2gmZZprime = on",
                "Zprime:gmZmode = 3"]
        elif abs(mom) == 24: [
                "WeakSingleBoson:ffbar2W = on"]
        else:
            self._next = lambda gen: gen.pgun()
            cfgs += ["ProcessLevel:all = off", "Check:event = off"]
        for cfg in cfgs: self.py.readString(cfg)
            
        # Configure the particle data.
        pdb  = self.py.particleData
        pdb.findParticle(18).clearChannels()
        decs = collections.OrderedDict()
        for lep in [11, 13, 15]: configure.decays(pdb, lep, mass, us, decs)
        for cfg in configure.configure(pdb, decs): self.py.readString(cfg)

        # Select the production channel.
        self.pros = collections.OrderedDict()
        pde  = pdb.findParticle(mom)
        for idx in range(0, pde.sizeChannels()):
            chn  = pde.channel(idx)
            dtrs = [chn.product(dtr) for dtr in range(0, chn.multiplicity())]
            if 18 in dtrs or -18 in dtrs: self.pros[idx] = dtrs
            if   idx != pro: chn.onMode(0)
            elif idx == pro and chn.bRatio() == 0: chn.bRatio(1e-3)
        
        # Select the decay channel.
        self.decs = collections.OrderedDict()
        pde = pdb.findParticle(18)
        for idx in range(0, pde.sizeChannels()):
            chn  = pde.channel(idx)
            dtrs = [chn.product(dtr) for dtr in range(0, chn.multiplicity())]
            self.decs[idx] = dtrs
            if   idx != dec: chn.onMode(0)
            elif idx == dec and chn.bRatio() == 0: chn.bRatio(1e-3)

        # Check if production and decay index known.
        if (pro < 0 and mom != 18) or dec < 0: return
        
        # Apply user post-configuration.
        if type(post) == str: self.py.readFile(post)
        elif post:
            for cfg in post: self.py.readString(cfg)

        # Intialize.
        self.init = self.py.init()
            
    #-------------------------------------------------------------------
    def next(self):
        """
        Generate the next event.
        """
        return self._next(self)
    
    #-------------------------------------------------------------------
    def pgun(self, theta = -1, phi = -1):
        """
        Generate a particle gun event, given the generator configuration.
        
        theta: polar angle, if < 0 generate in phase space.
        phi:   azimuthal angle, if < 0 generate in phase space.
        """
        from math import cos, sin, pi
        if not self.init: return False
        
        # Set kinematics.
        self.event = self.py.event
        self.event.reset()
        m = self.py.particleData.mSel(self.mom)
        e = max(self.com, m)
        p = 0 if m == e else (e*e - m*m)**0.5 

        # Angles as input or uniform in solid angle.
        if theta >= 0: ct, st = cos(theta), sin(theta)
        else:
            ct = 2*self.py.rndm.flat() - 1
            st = (1 - ct*ct)**0.5
        if phi < 0: phi = 2*pi*self.py.rndm.flat()
        
        # Store the particle in the event record and decay.
        idx = self.event.append(self.mom, 1, 0, 0, p * st * cos(phi),
                                   p * st * sin(phi), p * ct, e, m)
        self.event[idx].tau(self.event[idx].tau0() * self.py.rndm.exp())
        return self.py.next()

#===========================================================================
if __name__ == "__main__":
    """
    Main script. Runs some events.
    """
    # Parse arguments.
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument(
        "--mass", default = 0,
        help = "HNL mass in GeV.", type = float)
    parser.add_argument(
        "--ue", default = 0,
        help = "Electron mixing angle.", type = float)
    parser.add_argument(
        "--umu", default = 0,
        help = "Muon mixing angle.", type = float)
    parser.add_argument(
        "--utau", default = 0,
        help = "Tau mixing angle.", type = float)
    parser.add_argument(
        "--com", default = 20,
        help = "COM in GeV if vector mother, else mother energy..", type = int)
    parser.add_argument(
        "--mom", default = 511,
        help = "PID of the HNL mother.", type = int)
    parser.add_argument(
        "--pro", default = -1,
        help = "Production channel index.", type = int)
    parser.add_argument(
        "--dec", default = -1,
        help = "Decay channel index.", type = int)
    parser.add_argument(
        "--events", default = 10000,
        help = "Number of events to generate.", type = float)
    args = parser.parse_args()

    # Print production or decay channels if needed.
    gen = Generator(args.com, args.mom, args.mass,
                    [args.ue, args.umu, args.utau], args.pro, args.dec)
    pdb = gen.py.particleData
    if len(gen.pros) == 0 and args.mom != 18:
        print("Selected mother %s (%i) has no HNL decay channels." %
              (pdb.name(args.mom), args.mom))
        exit()
    if (args.pro < 0 and args.mom != 18) or args.dec < 0:
        if args.pro < 0 and args.mom != 18:
            print("-"*79)
            print("Available HNL production channels from the %s."
                  % pdb.name(args.mom))
            print("-"*79)
            for idx, dtrs in gen.pros.items(): print(
                    ("%4i " % idx) + " ".join([pdb.name(dtr) for dtr in dtrs]))
        if args.dec < 0:
            print("-"*79)
            print("Available decay channels for the HNL.")
            print("-"*79)
            for idx, dtrs in gen.decs.items(): print(
                    ("%4i " % idx) + " ".join([pdb.name(dtr) for dtr in dtrs]))
        exit()

    # Generate events.
    for evt in range(0, int(args.events)):
        gen.next()
    gen.py.stat()
    
