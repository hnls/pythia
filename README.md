# Resources

* https://github.com/sonia211/pythia8240_HNL
* https://github.com/janhajer/hnl
* https://arxiv.org/abs/1805.08567
* https://arxiv.org/abs/2103.11494

# Limitations

* *Not validated!*
* Neutral current interference not included in fully leptonic decays.

# Setup

```
cd pythia
./configure --with-python
make -j6
export PYTHONPATH=$PWD/lib
```

# Examples

```python
# Get configuration help.
./configure.py --help

# Generate a Pythia configuration file for a given mass and couplings.
./configure.py --mass=0.1 --ue=1
```

```python
# Get generation help.
./generate.py --help

# List available production and decay channels.
./generate.py --mass=4 --ue=1 --mom=511

# Generate some events.
./generate.py --mass=4 --ue=1 --mom=511 --pro=890 --dec=1
```

# Further Work

* Full helicity decay treatment of heavy meson decays.
* Recursive helicity decay structure.
  * Correlated tau decays from HNL decays.
  * HNL production from charged lepton decays.
* Include charged/neutral current interference in HNL -> l l nu.
* Add more neutral currents.
* Fully validate the code.
