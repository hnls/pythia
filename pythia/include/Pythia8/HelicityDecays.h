// HelicityDecays.h is a part of the PYTHIA event generator.
// Copyright (C) 2021 Philip Ilten, Torbjorn Sjostrand.
// PYTHIA is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// Header file for the HelicityDecays class.

#ifndef Pythia8_HelicityDecays_H
#define Pythia8_HelicityDecays_H

#include "Pythia8/Basics.h"
#include "Pythia8/Event.h"
#include "Pythia8/HelicityBasics.h"
#include "Pythia8/HelicityMatrixElements.h"
#include "Pythia8/PhysicsBase.h"
#include "Pythia8/PythiaComplex.h"
#include "Pythia8/PythiaStdlib.h"
#include "Pythia8/Settings.h"

namespace Pythia8 {

//==========================================================================

// HelicityDecays class.
// This class decays tau leptons, with helicity information.

class HelicityDecays : public PhysicsBase {

public:

  // Constructor and destructor.
  HelicityDecays() : correlated(), tauExt(), tauMode(), tauMother(), tauPol(),
    hardME(), decayME(), tau0Max(), tauMax(), rMax(), xyMax(), zMax(),
    limitTau0(), limitTau(), limitRadius(), limitCylinder(), limitDecay() {};
  ~HelicityDecays() {}

  // Initializer.
  void init();

  // Decay a tau or correlated tau pair.
  bool decay(int iDec, Event& event);

  // Determine internal or external polarization and correlation mechanism.
  bool internalMechanism(Event &event);
  bool externalMechanism(Event &event);

  // Choose a decay channel for a particle.
  vector<HelicityParticle> createChildren(HelicityParticle parent);

  // Perform an N-body isotropic decay.
  void isotropicDecay(vector<HelicityParticle>& p);

  // Write the decay to event record.
  void writeDecay(Event& event, vector<HelicityParticle>& p);

private:

  // Constants: could only be changed in the code itself.
  static const int    NTRYCHANNEL, NTRYDECAY;
  static const double WTCORRECTION[11];

  // Flag whether a correlated tau decay should be performed.
  bool correlated;

  // User selected mode and mother for tau decays.
  int tauExt, tauMode, tauMother, tauPol;

  // Helicity matrix element pointers.
  HelicityMatrixElement* hardME;
  HMEDecay* decayME;

  // Hard process helicity matrix elements.
  HMETwoFermions2W2TwoFermions      hmeTwoFermions2W2TwoFermions;
  HMETwoFermions2GammaZ2TwoFermions hmeTwoFermions2GammaZ2TwoFermions;
  HMETwoGammas2TwoFermions          hmeTwoGammas2TwoFermions;
  HMEW2TwoFermions                  hmeW2TwoFermions;
  HMEZ2TwoFermions                  hmeZ2TwoFermions;
  HMEGamma2TwoFermions              hmeGamma2TwoFermions;
  HMEHiggs2TwoFermions              hmeHiggs2TwoFermions;

  // Particles of the hard process.
  HelicityParticle in1, in2, mediator, out1, out2;
  vector<HelicityParticle> particles;

  // Parameters to determine if correlated partner should decay.
  double tau0Max, tauMax, rMax, xyMax, zMax;
  bool limitTau0, limitTau, limitRadius, limitCylinder, limitDecay;

};

//==========================================================================

} // end namespace Pythia8

#endif // Pythia8_HelicityDecays_H
