#include <Pythia8/Basics.h>
#include <Pythia8/BeamParticle.h>
#include <Pythia8/Event.h>
#include <Pythia8/HIUserHooks.h>
#include <Pythia8/HadronWidths.h>
#include <Pythia8/HeavyIons.h>
#include <Pythia8/Info.h>
#include <Pythia8/LHEF3.h>
#include <Pythia8/LesHouches.h>
#include <Pythia8/Merging.h>
#include <Pythia8/MergingHooks.h>
#include <Pythia8/ParticleData.h>
#include <Pythia8/ParticleDecays.h>
#include <Pythia8/PartonDistributions.h>
#include <Pythia8/PartonSystems.h>
#include <Pythia8/PartonVertex.h>
#include <Pythia8/PhaseSpace.h>
#include <Pythia8/Pythia.h>
#include <Pythia8/ResonanceWidths.h>
#include <Pythia8/Settings.h>
#include <Pythia8/SharedPointers.h>
#include <Pythia8/ShowerModel.h>
#include <Pythia8/SigmaProcess.h>
#include <Pythia8/SigmaTotal.h>
#include <Pythia8/StandardModel.h>
#include <Pythia8/SusyCouplings.h>
#include <Pythia8/UserHooks.h>
#include <Pythia8/Weights.h>
#include <functional>
#include <ios>
#include <istream>
#include <iterator>
#include <map>
#include <memory>
#include <sstream> // __str__
#include <streambuf>
#include <string>
#include <utility>
#include <vector>

#include <pybind11/pybind11.h>
#include <functional>
#include <string>
#include <Pythia8/UserHooks.h>
#include <Pythia8/HIUserHooks.h>
#include <Pythia8/HeavyIons.h>
#include <Pythia8/BeamShape.h>
#include <pybind11/stl.h>
#include <pybind11/complex.h>


#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
	PYBIND11_MAKE_OPAQUE(std::shared_ptr<void>);
#endif

// Pythia8::LHAupLHEF file:Pythia8/LesHouches.h line:344
struct PyCallBack_Pythia8_LHAupLHEF : public Pythia8::LHAupLHEF {
	using Pythia8::LHAupLHEF::LHAupLHEF;

	void newEventFile(const char * a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::LHAupLHEF *>(this), "newEventFile");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return LHAupLHEF::newEventFile(a0);
	}
	bool fileFound() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::LHAupLHEF *>(this), "fileFound");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return LHAupLHEF::fileFound();
	}
	bool useExternal() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::LHAupLHEF *>(this), "useExternal");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return LHAupLHEF::useExternal();
	}
	bool setInit() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::LHAupLHEF *>(this), "setInit");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return LHAupLHEF::setInit();
	}
	bool setEvent(int a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::LHAupLHEF *>(this), "setEvent");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return LHAupLHEF::setEvent(a0);
	}
	bool skipEvent(int a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::LHAupLHEF *>(this), "skipEvent");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return LHAupLHEF::skipEvent(a0);
	}
	bool openLHEF(class std::basic_string<char> a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::LHAupLHEF *>(this), "openLHEF");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return LHAup::openLHEF(a0);
	}
	bool closeLHEF(bool a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::LHAupLHEF *>(this), "closeLHEF");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return LHAup::closeLHEF(a0);
	}
};

// Pythia8::LHAupPlugin file:Pythia8/LesHouches.h line:482
struct PyCallBack_Pythia8_LHAupPlugin : public Pythia8::LHAupPlugin {
	using Pythia8::LHAupPlugin::LHAupPlugin;

	bool setInit() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::LHAupPlugin *>(this), "setInit");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return LHAupPlugin::setInit();
	}
	bool setEvent(int a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::LHAupPlugin *>(this), "setEvent");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return LHAupPlugin::setEvent(a0);
	}
	void newEventFile(const char * a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::LHAupPlugin *>(this), "newEventFile");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return LHAup::newEventFile(a0);
	}
	bool fileFound() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::LHAupPlugin *>(this), "fileFound");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return LHAup::fileFound();
	}
	bool useExternal() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::LHAupPlugin *>(this), "useExternal");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return LHAup::useExternal();
	}
	bool skipEvent(int a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::LHAupPlugin *>(this), "skipEvent");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return LHAup::skipEvent(a0);
	}
	bool openLHEF(class std::basic_string<char> a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::LHAupPlugin *>(this), "openLHEF");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return LHAup::openLHEF(a0);
	}
	bool closeLHEF(bool a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::LHAupPlugin *>(this), "closeLHEF");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return LHAup::closeLHEF(a0);
	}
};

// Pythia8::LHAupFromPYTHIA8 file:Pythia8/LesHouches.h line:514
struct PyCallBack_Pythia8_LHAupFromPYTHIA8 : public Pythia8::LHAupFromPYTHIA8 {
	using Pythia8::LHAupFromPYTHIA8::LHAupFromPYTHIA8;

	bool setInit() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::LHAupFromPYTHIA8 *>(this), "setInit");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return LHAupFromPYTHIA8::setInit();
	}
	bool setEvent(int a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::LHAupFromPYTHIA8 *>(this), "setEvent");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return LHAupFromPYTHIA8::setEvent(a0);
	}
	void newEventFile(const char * a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::LHAupFromPYTHIA8 *>(this), "newEventFile");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return LHAup::newEventFile(a0);
	}
	bool fileFound() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::LHAupFromPYTHIA8 *>(this), "fileFound");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return LHAup::fileFound();
	}
	bool useExternal() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::LHAupFromPYTHIA8 *>(this), "useExternal");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return LHAup::useExternal();
	}
	bool skipEvent(int a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::LHAupFromPYTHIA8 *>(this), "skipEvent");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return LHAup::skipEvent(a0);
	}
	bool openLHEF(class std::basic_string<char> a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::LHAupFromPYTHIA8 *>(this), "openLHEF");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return LHAup::openLHEF(a0);
	}
	bool closeLHEF(bool a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::LHAupFromPYTHIA8 *>(this), "closeLHEF");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return LHAup::closeLHEF(a0);
	}
};

// Pythia8::LHEF3FromPythia8 file:Pythia8/LesHouches.h line:548
struct PyCallBack_Pythia8_LHEF3FromPythia8 : public Pythia8::LHEF3FromPythia8 {
	using Pythia8::LHEF3FromPythia8::LHEF3FromPythia8;

	bool setInit() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::LHEF3FromPythia8 *>(this), "setInit");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return LHEF3FromPythia8::setInit();
	}
	bool setEvent(int a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::LHEF3FromPythia8 *>(this), "setEvent");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return LHEF3FromPythia8::setEvent(a0);
	}
	bool openLHEF(class std::basic_string<char> a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::LHEF3FromPythia8 *>(this), "openLHEF");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return LHEF3FromPythia8::openLHEF(a0);
	}
	bool closeLHEF(bool a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::LHEF3FromPythia8 *>(this), "closeLHEF");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return LHEF3FromPythia8::closeLHEF(a0);
	}
	void newEventFile(const char * a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::LHEF3FromPythia8 *>(this), "newEventFile");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return LHAup::newEventFile(a0);
	}
	bool fileFound() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::LHEF3FromPythia8 *>(this), "fileFound");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return LHAup::fileFound();
	}
	bool useExternal() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::LHEF3FromPythia8 *>(this), "useExternal");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return LHAup::useExternal();
	}
	bool skipEvent(int a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::LHEF3FromPythia8 *>(this), "skipEvent");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return LHAup::skipEvent(a0);
	}
};

void bind_Pythia8_LesHouches_1(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	{ // Pythia8::LHAupLHEF file:Pythia8/LesHouches.h line:344
		pybind11::class_<Pythia8::LHAupLHEF, std::shared_ptr<Pythia8::LHAupLHEF>, PyCallBack_Pythia8_LHAupLHEF, Pythia8::LHAup> cl(M("Pythia8"), "LHAupLHEF", "");
		pybind11::handle cl_type = cl;

		cl.def( pybind11::init( [](class Pythia8::Info * a0, class std::basic_istream<char> * a1, class std::basic_istream<char> * a2){ return new Pythia8::LHAupLHEF(a0, a1, a2); }, [](class Pythia8::Info * a0, class std::basic_istream<char> * a1, class std::basic_istream<char> * a2){ return new PyCallBack_Pythia8_LHAupLHEF(a0, a1, a2); } ), "doc");
		cl.def( pybind11::init( [](class Pythia8::Info * a0, class std::basic_istream<char> * a1, class std::basic_istream<char> * a2, bool const & a3){ return new Pythia8::LHAupLHEF(a0, a1, a2, a3); }, [](class Pythia8::Info * a0, class std::basic_istream<char> * a1, class std::basic_istream<char> * a2, bool const & a3){ return new PyCallBack_Pythia8_LHAupLHEF(a0, a1, a2, a3); } ), "doc");
		cl.def( pybind11::init<class Pythia8::Info *, class std::basic_istream<char> *, class std::basic_istream<char> *, bool, bool>(), pybind11::arg("infoPtrIn"), pybind11::arg("isIn"), pybind11::arg("isHeadIn"), pybind11::arg("readHeadersIn"), pybind11::arg("setScalesFromLHEFIn") );

		cl.def( pybind11::init( [](class Pythia8::Info * a0, const char * a1){ return new Pythia8::LHAupLHEF(a0, a1); }, [](class Pythia8::Info * a0, const char * a1){ return new PyCallBack_Pythia8_LHAupLHEF(a0, a1); } ), "doc");
		cl.def( pybind11::init( [](class Pythia8::Info * a0, const char * a1, const char * a2){ return new Pythia8::LHAupLHEF(a0, a1, a2); }, [](class Pythia8::Info * a0, const char * a1, const char * a2){ return new PyCallBack_Pythia8_LHAupLHEF(a0, a1, a2); } ), "doc");
		cl.def( pybind11::init( [](class Pythia8::Info * a0, const char * a1, const char * a2, bool const & a3){ return new Pythia8::LHAupLHEF(a0, a1, a2, a3); }, [](class Pythia8::Info * a0, const char * a1, const char * a2, bool const & a3){ return new PyCallBack_Pythia8_LHAupLHEF(a0, a1, a2, a3); } ), "doc");
		cl.def( pybind11::init<class Pythia8::Info *, const char *, const char *, bool, bool>(), pybind11::arg("infoPtrIn"), pybind11::arg("filenameIn"), pybind11::arg("headerIn"), pybind11::arg("readHeadersIn"), pybind11::arg("setScalesFromLHEFIn") );

		cl.def("closeAllFiles", (void (Pythia8::LHAupLHEF::*)()) &Pythia8::LHAupLHEF::closeAllFiles, "C++: Pythia8::LHAupLHEF::closeAllFiles() --> void");
		cl.def("newEventFile", (void (Pythia8::LHAupLHEF::*)(const char *)) &Pythia8::LHAupLHEF::newEventFile, "C++: Pythia8::LHAupLHEF::newEventFile(const char *) --> void", pybind11::arg("filenameIn"));
		cl.def("fileFound", (bool (Pythia8::LHAupLHEF::*)()) &Pythia8::LHAupLHEF::fileFound, "C++: Pythia8::LHAupLHEF::fileFound() --> bool");
		cl.def("useExternal", (bool (Pythia8::LHAupLHEF::*)()) &Pythia8::LHAupLHEF::useExternal, "C++: Pythia8::LHAupLHEF::useExternal() --> bool");
		cl.def("setInit", (bool (Pythia8::LHAupLHEF::*)()) &Pythia8::LHAupLHEF::setInit, "C++: Pythia8::LHAupLHEF::setInit() --> bool");
		cl.def("setInitLHEF", (bool (Pythia8::LHAupLHEF::*)(class std::basic_istream<char> &, bool)) &Pythia8::LHAupLHEF::setInitLHEF, "C++: Pythia8::LHAupLHEF::setInitLHEF(class std::basic_istream<char> &, bool) --> bool", pybind11::arg("isIn"), pybind11::arg("readHead"));
		cl.def("setEvent", [](Pythia8::LHAupLHEF &o) -> bool { return o.setEvent(); }, "");
		cl.def("setEvent", (bool (Pythia8::LHAupLHEF::*)(int)) &Pythia8::LHAupLHEF::setEvent, "C++: Pythia8::LHAupLHEF::setEvent(int) --> bool", pybind11::arg(""));
		cl.def("skipEvent", (bool (Pythia8::LHAupLHEF::*)(int)) &Pythia8::LHAupLHEF::skipEvent, "C++: Pythia8::LHAupLHEF::skipEvent(int) --> bool", pybind11::arg("nSkip"));
		cl.def("setNewEventLHEF", (bool (Pythia8::LHAupLHEF::*)()) &Pythia8::LHAupLHEF::setNewEventLHEF, "C++: Pythia8::LHAupLHEF::setNewEventLHEF() --> bool");
		cl.def("updateSigma", (bool (Pythia8::LHAupLHEF::*)()) &Pythia8::LHAupLHEF::updateSigma, "C++: Pythia8::LHAupLHEF::updateSigma() --> bool");
		cl.def("getLine", [](Pythia8::LHAupLHEF &o, class std::basic_string<char> & a0) -> bool { return o.getLine(a0); }, "", pybind11::arg("line"));
		cl.def("getLine", (bool (Pythia8::LHAupLHEF::*)(std::string &, bool)) &Pythia8::LHAupLHEF::getLine, "C++: Pythia8::LHAupLHEF::getLine(std::string &, bool) --> bool", pybind11::arg("line"), pybind11::arg("header"));
	}
	{ // Pythia8::LHAupPlugin file:Pythia8/LesHouches.h line:482
		pybind11::class_<Pythia8::LHAupPlugin, std::shared_ptr<Pythia8::LHAupPlugin>, PyCallBack_Pythia8_LHAupPlugin, Pythia8::LHAup> cl(M("Pythia8"), "LHAupPlugin", "");
		pybind11::handle cl_type = cl;

		cl.def( pybind11::init( [](){ return new Pythia8::LHAupPlugin(); }, [](){ return new PyCallBack_Pythia8_LHAupPlugin(); } ), "doc");
		cl.def( pybind11::init( [](class std::basic_string<char> const & a0){ return new Pythia8::LHAupPlugin(a0); }, [](class std::basic_string<char> const & a0){ return new PyCallBack_Pythia8_LHAupPlugin(a0); } ), "doc");
		cl.def( pybind11::init<std::string, class Pythia8::Pythia *>(), pybind11::arg("nameIn"), pybind11::arg("pythiaPtr") );

		cl.def("setInit", (bool (Pythia8::LHAupPlugin::*)()) &Pythia8::LHAupPlugin::setInit, "C++: Pythia8::LHAupPlugin::setInit() --> bool");
		cl.def("setEvent", [](Pythia8::LHAupPlugin &o) -> bool { return o.setEvent(); }, "");
		cl.def("setEvent", (bool (Pythia8::LHAupPlugin::*)(int)) &Pythia8::LHAupPlugin::setEvent, "C++: Pythia8::LHAupPlugin::setEvent(int) --> bool", pybind11::arg("idProcIn"));
	}
	{ // Pythia8::LHAupFromPYTHIA8 file:Pythia8/LesHouches.h line:514
		pybind11::class_<Pythia8::LHAupFromPYTHIA8, std::shared_ptr<Pythia8::LHAupFromPYTHIA8>, PyCallBack_Pythia8_LHAupFromPYTHIA8, Pythia8::LHAup> cl(M("Pythia8"), "LHAupFromPYTHIA8", "");
		pybind11::handle cl_type = cl;

		cl.def( pybind11::init<class Pythia8::Event *, const class Pythia8::Pythia *>(), pybind11::arg("processPtrIn"), pybind11::arg("pythia") );

		cl.def("setInit", (bool (Pythia8::LHAupFromPYTHIA8::*)()) &Pythia8::LHAupFromPYTHIA8::setInit, "C++: Pythia8::LHAupFromPYTHIA8::setInit() --> bool");
		cl.def("setEvent", [](Pythia8::LHAupFromPYTHIA8 &o) -> bool { return o.setEvent(); }, "");
		cl.def("setEvent", (bool (Pythia8::LHAupFromPYTHIA8::*)(int)) &Pythia8::LHAupFromPYTHIA8::setEvent, "C++: Pythia8::LHAupFromPYTHIA8::setEvent(int) --> bool", pybind11::arg(""));
		cl.def("updateSigma", (bool (Pythia8::LHAupFromPYTHIA8::*)()) &Pythia8::LHAupFromPYTHIA8::updateSigma, "C++: Pythia8::LHAupFromPYTHIA8::updateSigma() --> bool");
	}
	{ // Pythia8::LHEF3FromPythia8 file:Pythia8/LesHouches.h line:548
		pybind11::class_<Pythia8::LHEF3FromPythia8, std::shared_ptr<Pythia8::LHEF3FromPythia8>, PyCallBack_Pythia8_LHEF3FromPythia8, Pythia8::LHAup> cl(M("Pythia8"), "LHEF3FromPythia8", "");
		pybind11::handle cl_type = cl;

		cl.def( pybind11::init( [](class Pythia8::Event * a0, const class Pythia8::Info * a1){ return new Pythia8::LHEF3FromPythia8(a0, a1); }, [](class Pythia8::Event * a0, const class Pythia8::Info * a1){ return new PyCallBack_Pythia8_LHEF3FromPythia8(a0, a1); } ), "doc");
		cl.def( pybind11::init( [](class Pythia8::Event * a0, const class Pythia8::Info * a1, int const & a2){ return new Pythia8::LHEF3FromPythia8(a0, a1, a2); }, [](class Pythia8::Event * a0, const class Pythia8::Info * a1, int const & a2){ return new PyCallBack_Pythia8_LHEF3FromPythia8(a0, a1, a2); } ), "doc");
		cl.def( pybind11::init<class Pythia8::Event *, const class Pythia8::Info *, int, bool>(), pybind11::arg("eventPtrIn"), pybind11::arg("infoPtrIn"), pybind11::arg("pDigitsIn"), pybind11::arg("writeToFileIn") );

		cl.def("setInit", (bool (Pythia8::LHEF3FromPythia8::*)()) &Pythia8::LHEF3FromPythia8::setInit, "C++: Pythia8::LHEF3FromPythia8::setInit() --> bool");
		cl.def("setEventPtr", (void (Pythia8::LHEF3FromPythia8::*)(class Pythia8::Event *)) &Pythia8::LHEF3FromPythia8::setEventPtr, "C++: Pythia8::LHEF3FromPythia8::setEventPtr(class Pythia8::Event *) --> void", pybind11::arg("evPtr"));
		cl.def("setEvent", [](Pythia8::LHEF3FromPythia8 &o) -> bool { return o.setEvent(); }, "");
		cl.def("setEvent", (bool (Pythia8::LHEF3FromPythia8::*)(int)) &Pythia8::LHEF3FromPythia8::setEvent, "C++: Pythia8::LHEF3FromPythia8::setEvent(int) --> bool", pybind11::arg(""));
		cl.def("getEventString", (std::string (Pythia8::LHEF3FromPythia8::*)()) &Pythia8::LHEF3FromPythia8::getEventString, "C++: Pythia8::LHEF3FromPythia8::getEventString() --> std::string");
		cl.def("openLHEF", (bool (Pythia8::LHEF3FromPythia8::*)(std::string)) &Pythia8::LHEF3FromPythia8::openLHEF, "C++: Pythia8::LHEF3FromPythia8::openLHEF(std::string) --> bool", pybind11::arg("fileNameIn"));
		cl.def("closeLHEF", [](Pythia8::LHEF3FromPythia8 &o) -> bool { return o.closeLHEF(); }, "");
		cl.def("closeLHEF", (bool (Pythia8::LHEF3FromPythia8::*)(bool)) &Pythia8::LHEF3FromPythia8::closeLHEF, "C++: Pythia8::LHEF3FromPythia8::closeLHEF(bool) --> bool", pybind11::arg("updateInit"));
	}
}
