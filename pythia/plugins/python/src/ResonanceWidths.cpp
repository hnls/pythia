#include <Pythia8/Basics.h>
#include <Pythia8/BeamParticle.h>
#include <Pythia8/Event.h>
#include <Pythia8/FragmentationFlavZpT.h>
#include <Pythia8/HadronWidths.h>
#include <Pythia8/Info.h>
#include <Pythia8/LHEF3.h>
#include <Pythia8/ParticleData.h>
#include <Pythia8/PartonDistributions.h>
#include <Pythia8/PartonSystems.h>
#include <Pythia8/ResonanceWidths.h>
#include <Pythia8/Settings.h>
#include <Pythia8/SigmaTotal.h>
#include <Pythia8/StandardModel.h>
#include <Pythia8/SusyCouplings.h>
#include <Pythia8/SusyLesHouches.h>
#include <Pythia8/Weights.h>
#include <complex>
#include <functional>
#include <istream>
#include <iterator>
#include <map>
#include <memory>
#include <ostream>
#include <sstream> // __str__
#include <string>
#include <utility>
#include <vector>

#include <pybind11/pybind11.h>
#include <functional>
#include <string>
#include <Pythia8/UserHooks.h>
#include <Pythia8/HIUserHooks.h>
#include <Pythia8/HeavyIons.h>
#include <Pythia8/BeamShape.h>
#include <pybind11/stl.h>
#include <pybind11/complex.h>


#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
	PYBIND11_MAKE_OPAQUE(std::shared_ptr<void>);
#endif

// Pythia8::ResonanceWidths file:Pythia8/ResonanceWidths.h line:34
struct PyCallBack_Pythia8_ResonanceWidths : public Pythia8::ResonanceWidths {
	using Pythia8::ResonanceWidths::ResonanceWidths;

	bool init(class Pythia8::Info * a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceWidths *>(this), "init");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return ResonanceWidths::init(a0);
	}
	void initConstants() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceWidths *>(this), "initConstants");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return ResonanceWidths::initConstants();
	}
	bool initBSM() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceWidths *>(this), "initBSM");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return ResonanceWidths::initBSM();
	}
	bool allowCalc() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceWidths *>(this), "allowCalc");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return ResonanceWidths::allowCalc();
	}
	void calcPreFac(bool a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceWidths *>(this), "calcPreFac");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return ResonanceWidths::calcPreFac(a0);
	}
	void calcWidth(bool a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceWidths *>(this), "calcWidth");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return ResonanceWidths::calcWidth(a0);
	}
};

// Pythia8::ResonanceGeneric file:Pythia8/ResonanceWidths.h line:149
struct PyCallBack_Pythia8_ResonanceGeneric : public Pythia8::ResonanceGeneric {
	using Pythia8::ResonanceGeneric::ResonanceGeneric;

	bool allowCalc() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceGeneric *>(this), "allowCalc");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return ResonanceGeneric::allowCalc();
	}
	bool init(class Pythia8::Info * a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceGeneric *>(this), "init");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return ResonanceWidths::init(a0);
	}
	void initConstants() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceGeneric *>(this), "initConstants");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return ResonanceWidths::initConstants();
	}
	bool initBSM() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceGeneric *>(this), "initBSM");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return ResonanceWidths::initBSM();
	}
	void calcPreFac(bool a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceGeneric *>(this), "calcPreFac");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return ResonanceWidths::calcPreFac(a0);
	}
	void calcWidth(bool a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceGeneric *>(this), "calcWidth");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return ResonanceWidths::calcWidth(a0);
	}
};

// Pythia8::ResonanceGmZ file:Pythia8/ResonanceWidths.h line:165
struct PyCallBack_Pythia8_ResonanceGmZ : public Pythia8::ResonanceGmZ {
	using Pythia8::ResonanceGmZ::ResonanceGmZ;

	bool init(class Pythia8::Info * a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceGmZ *>(this), "init");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return ResonanceWidths::init(a0);
	}
	void initConstants() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceGmZ *>(this), "initConstants");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return ResonanceWidths::initConstants();
	}
	bool initBSM() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceGmZ *>(this), "initBSM");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return ResonanceWidths::initBSM();
	}
	bool allowCalc() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceGmZ *>(this), "allowCalc");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return ResonanceWidths::allowCalc();
	}
	void calcPreFac(bool a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceGmZ *>(this), "calcPreFac");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return ResonanceWidths::calcPreFac(a0);
	}
	void calcWidth(bool a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceGmZ *>(this), "calcWidth");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return ResonanceWidths::calcWidth(a0);
	}
};

// Pythia8::ResonanceW file:Pythia8/ResonanceWidths.h line:194
struct PyCallBack_Pythia8_ResonanceW : public Pythia8::ResonanceW {
	using Pythia8::ResonanceW::ResonanceW;

	bool init(class Pythia8::Info * a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceW *>(this), "init");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return ResonanceWidths::init(a0);
	}
	void initConstants() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceW *>(this), "initConstants");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return ResonanceWidths::initConstants();
	}
	bool initBSM() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceW *>(this), "initBSM");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return ResonanceWidths::initBSM();
	}
	bool allowCalc() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceW *>(this), "allowCalc");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return ResonanceWidths::allowCalc();
	}
	void calcPreFac(bool a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceW *>(this), "calcPreFac");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return ResonanceWidths::calcPreFac(a0);
	}
	void calcWidth(bool a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceW *>(this), "calcWidth");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return ResonanceWidths::calcWidth(a0);
	}
};

// Pythia8::ResonanceTop file:Pythia8/ResonanceWidths.h line:221
struct PyCallBack_Pythia8_ResonanceTop : public Pythia8::ResonanceTop {
	using Pythia8::ResonanceTop::ResonanceTop;

	bool init(class Pythia8::Info * a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceTop *>(this), "init");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return ResonanceWidths::init(a0);
	}
	void initConstants() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceTop *>(this), "initConstants");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return ResonanceWidths::initConstants();
	}
	bool initBSM() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceTop *>(this), "initBSM");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return ResonanceWidths::initBSM();
	}
	bool allowCalc() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceTop *>(this), "allowCalc");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return ResonanceWidths::allowCalc();
	}
	void calcPreFac(bool a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceTop *>(this), "calcPreFac");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return ResonanceWidths::calcPreFac(a0);
	}
	void calcWidth(bool a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceTop *>(this), "calcWidth");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return ResonanceWidths::calcWidth(a0);
	}
};

// Pythia8::ResonanceFour file:Pythia8/ResonanceWidths.h line:249
struct PyCallBack_Pythia8_ResonanceFour : public Pythia8::ResonanceFour {
	using Pythia8::ResonanceFour::ResonanceFour;

	bool init(class Pythia8::Info * a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceFour *>(this), "init");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return ResonanceWidths::init(a0);
	}
	void initConstants() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceFour *>(this), "initConstants");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return ResonanceWidths::initConstants();
	}
	bool initBSM() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceFour *>(this), "initBSM");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return ResonanceWidths::initBSM();
	}
	bool allowCalc() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceFour *>(this), "allowCalc");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return ResonanceWidths::allowCalc();
	}
	void calcPreFac(bool a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceFour *>(this), "calcPreFac");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return ResonanceWidths::calcPreFac(a0);
	}
	void calcWidth(bool a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceFour *>(this), "calcWidth");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return ResonanceWidths::calcWidth(a0);
	}
};

// Pythia8::ResonanceH file:Pythia8/ResonanceWidths.h line:277
struct PyCallBack_Pythia8_ResonanceH : public Pythia8::ResonanceH {
	using Pythia8::ResonanceH::ResonanceH;

	bool init(class Pythia8::Info * a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceH *>(this), "init");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return ResonanceWidths::init(a0);
	}
	void initConstants() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceH *>(this), "initConstants");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return ResonanceWidths::initConstants();
	}
	bool initBSM() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceH *>(this), "initBSM");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return ResonanceWidths::initBSM();
	}
	bool allowCalc() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceH *>(this), "allowCalc");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return ResonanceWidths::allowCalc();
	}
	void calcPreFac(bool a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceH *>(this), "calcPreFac");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return ResonanceWidths::calcPreFac(a0);
	}
	void calcWidth(bool a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceH *>(this), "calcWidth");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return ResonanceWidths::calcWidth(a0);
	}
};

// Pythia8::ResonanceHchg file:Pythia8/ResonanceWidths.h line:330
struct PyCallBack_Pythia8_ResonanceHchg : public Pythia8::ResonanceHchg {
	using Pythia8::ResonanceHchg::ResonanceHchg;

	bool init(class Pythia8::Info * a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceHchg *>(this), "init");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return ResonanceWidths::init(a0);
	}
	void initConstants() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceHchg *>(this), "initConstants");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return ResonanceWidths::initConstants();
	}
	bool initBSM() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceHchg *>(this), "initBSM");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return ResonanceWidths::initBSM();
	}
	bool allowCalc() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceHchg *>(this), "allowCalc");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return ResonanceWidths::allowCalc();
	}
	void calcPreFac(bool a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceHchg *>(this), "calcPreFac");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return ResonanceWidths::calcPreFac(a0);
	}
	void calcWidth(bool a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceHchg *>(this), "calcWidth");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return ResonanceWidths::calcWidth(a0);
	}
};

// Pythia8::ResonanceZprime file:Pythia8/ResonanceWidths.h line:359
struct PyCallBack_Pythia8_ResonanceZprime : public Pythia8::ResonanceZprime {
	using Pythia8::ResonanceZprime::ResonanceZprime;

	bool init(class Pythia8::Info * a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceZprime *>(this), "init");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return ResonanceWidths::init(a0);
	}
	void initConstants() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceZprime *>(this), "initConstants");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return ResonanceWidths::initConstants();
	}
	bool initBSM() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceZprime *>(this), "initBSM");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return ResonanceWidths::initBSM();
	}
	bool allowCalc() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceZprime *>(this), "allowCalc");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return ResonanceWidths::allowCalc();
	}
	void calcPreFac(bool a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceZprime *>(this), "calcPreFac");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return ResonanceWidths::calcPreFac(a0);
	}
	void calcWidth(bool a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceZprime *>(this), "calcWidth");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return ResonanceWidths::calcWidth(a0);
	}
};

// Pythia8::ResonanceWprime file:Pythia8/ResonanceWidths.h line:392
struct PyCallBack_Pythia8_ResonanceWprime : public Pythia8::ResonanceWprime {
	using Pythia8::ResonanceWprime::ResonanceWprime;

	bool init(class Pythia8::Info * a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceWprime *>(this), "init");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return ResonanceWidths::init(a0);
	}
	void initConstants() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceWprime *>(this), "initConstants");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return ResonanceWidths::initConstants();
	}
	bool initBSM() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceWprime *>(this), "initBSM");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return ResonanceWidths::initBSM();
	}
	bool allowCalc() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceWprime *>(this), "allowCalc");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::overload_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return ResonanceWidths::allowCalc();
	}
	void calcPreFac(bool a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceWprime *>(this), "calcPreFac");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return ResonanceWidths::calcPreFac(a0);
	}
	void calcWidth(bool a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const Pythia8::ResonanceWprime *>(this), "calcWidth");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return ResonanceWidths::calcWidth(a0);
	}
};

void bind_Pythia8_ResonanceWidths(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	{ // Pythia8::ResonanceWidths file:Pythia8/ResonanceWidths.h line:34
		pybind11::class_<Pythia8::ResonanceWidths, std::shared_ptr<Pythia8::ResonanceWidths>, PyCallBack_Pythia8_ResonanceWidths> cl(M("Pythia8"), "ResonanceWidths", "");
		pybind11::handle cl_type = cl;

		cl.def( pybind11::init( [](){ return new Pythia8::ResonanceWidths(); }, [](){ return new PyCallBack_Pythia8_ResonanceWidths(); } ) );
		cl.def( pybind11::init( [](PyCallBack_Pythia8_ResonanceWidths const &o){ return new PyCallBack_Pythia8_ResonanceWidths(o); } ) );
		cl.def( pybind11::init( [](Pythia8::ResonanceWidths const &o){ return new Pythia8::ResonanceWidths(o); } ) );
		cl.def_readwrite("idRes", &Pythia8::ResonanceWidths::idRes);
		cl.def_readwrite("hasAntiRes", &Pythia8::ResonanceWidths::hasAntiRes);
		cl.def_readwrite("doForceWidth", &Pythia8::ResonanceWidths::doForceWidth);
		cl.def_readwrite("isGeneric", &Pythia8::ResonanceWidths::isGeneric);
		cl.def_readwrite("allowCalcWidth", &Pythia8::ResonanceWidths::allowCalcWidth);
		cl.def_readwrite("minWidth", &Pythia8::ResonanceWidths::minWidth);
		cl.def_readwrite("minThreshold", &Pythia8::ResonanceWidths::minThreshold);
		cl.def_readwrite("mRes", &Pythia8::ResonanceWidths::mRes);
		cl.def_readwrite("GammaRes", &Pythia8::ResonanceWidths::GammaRes);
		cl.def_readwrite("m2Res", &Pythia8::ResonanceWidths::m2Res);
		cl.def_readwrite("GamMRat", &Pythia8::ResonanceWidths::GamMRat);
		cl.def_readwrite("openPos", &Pythia8::ResonanceWidths::openPos);
		cl.def_readwrite("openNeg", &Pythia8::ResonanceWidths::openNeg);
		cl.def_readwrite("forceFactor", &Pythia8::ResonanceWidths::forceFactor);
		cl.def_readwrite("iChannel", &Pythia8::ResonanceWidths::iChannel);
		cl.def_readwrite("onMode", &Pythia8::ResonanceWidths::onMode);
		cl.def_readwrite("meMode", &Pythia8::ResonanceWidths::meMode);
		cl.def_readwrite("mult", &Pythia8::ResonanceWidths::mult);
		cl.def_readwrite("id1", &Pythia8::ResonanceWidths::id1);
		cl.def_readwrite("id2", &Pythia8::ResonanceWidths::id2);
		cl.def_readwrite("id3", &Pythia8::ResonanceWidths::id3);
		cl.def_readwrite("id1Abs", &Pythia8::ResonanceWidths::id1Abs);
		cl.def_readwrite("id2Abs", &Pythia8::ResonanceWidths::id2Abs);
		cl.def_readwrite("id3Abs", &Pythia8::ResonanceWidths::id3Abs);
		cl.def_readwrite("idInFlav", &Pythia8::ResonanceWidths::idInFlav);
		cl.def_readwrite("widNow", &Pythia8::ResonanceWidths::widNow);
		cl.def_readwrite("mHat", &Pythia8::ResonanceWidths::mHat);
		cl.def_readwrite("mf1", &Pythia8::ResonanceWidths::mf1);
		cl.def_readwrite("mf2", &Pythia8::ResonanceWidths::mf2);
		cl.def_readwrite("mf3", &Pythia8::ResonanceWidths::mf3);
		cl.def_readwrite("mr1", &Pythia8::ResonanceWidths::mr1);
		cl.def_readwrite("mr2", &Pythia8::ResonanceWidths::mr2);
		cl.def_readwrite("mr3", &Pythia8::ResonanceWidths::mr3);
		cl.def_readwrite("ps", &Pythia8::ResonanceWidths::ps);
		cl.def_readwrite("kinFac", &Pythia8::ResonanceWidths::kinFac);
		cl.def_readwrite("alpEM", &Pythia8::ResonanceWidths::alpEM);
		cl.def_readwrite("alpS", &Pythia8::ResonanceWidths::alpS);
		cl.def_readwrite("colQ", &Pythia8::ResonanceWidths::colQ);
		cl.def_readwrite("preFac", &Pythia8::ResonanceWidths::preFac);
		cl.def_readwrite("particlePtr", &Pythia8::ResonanceWidths::particlePtr);
		cl.def("initBasic", [](Pythia8::ResonanceWidths &o, int const & a0) -> void { return o.initBasic(a0); }, "", pybind11::arg("idResIn"));
		cl.def("initBasic", (void (Pythia8::ResonanceWidths::*)(int, bool)) &Pythia8::ResonanceWidths::initBasic, "C++: Pythia8::ResonanceWidths::initBasic(int, bool) --> void", pybind11::arg("idResIn"), pybind11::arg("isGenericIn"));
		cl.def("init", (bool (Pythia8::ResonanceWidths::*)(class Pythia8::Info *)) &Pythia8::ResonanceWidths::init, "C++: Pythia8::ResonanceWidths::init(class Pythia8::Info *) --> bool", pybind11::arg("infoPtrIn"));
		cl.def("id", (int (Pythia8::ResonanceWidths::*)() const) &Pythia8::ResonanceWidths::id, "C++: Pythia8::ResonanceWidths::id() const --> int");
		cl.def("width", [](Pythia8::ResonanceWidths &o, int const & a0, double const & a1) -> double { return o.width(a0, a1); }, "", pybind11::arg("idSgn"), pybind11::arg("mHatIn"));
		cl.def("width", [](Pythia8::ResonanceWidths &o, int const & a0, double const & a1, int const & a2) -> double { return o.width(a0, a1, a2); }, "", pybind11::arg("idSgn"), pybind11::arg("mHatIn"), pybind11::arg("idInFlavIn"));
		cl.def("width", [](Pythia8::ResonanceWidths &o, int const & a0, double const & a1, int const & a2, bool const & a3) -> double { return o.width(a0, a1, a2, a3); }, "", pybind11::arg("idSgn"), pybind11::arg("mHatIn"), pybind11::arg("idInFlavIn"), pybind11::arg("openOnly"));
		cl.def("width", [](Pythia8::ResonanceWidths &o, int const & a0, double const & a1, int const & a2, bool const & a3, bool const & a4) -> double { return o.width(a0, a1, a2, a3, a4); }, "", pybind11::arg("idSgn"), pybind11::arg("mHatIn"), pybind11::arg("idInFlavIn"), pybind11::arg("openOnly"), pybind11::arg("setBR"));
		cl.def("width", [](Pythia8::ResonanceWidths &o, int const & a0, double const & a1, int const & a2, bool const & a3, bool const & a4, int const & a5) -> double { return o.width(a0, a1, a2, a3, a4, a5); }, "", pybind11::arg("idSgn"), pybind11::arg("mHatIn"), pybind11::arg("idInFlavIn"), pybind11::arg("openOnly"), pybind11::arg("setBR"), pybind11::arg("idOutFlav1"));
		cl.def("width", (double (Pythia8::ResonanceWidths::*)(int, double, int, bool, bool, int, int)) &Pythia8::ResonanceWidths::width, "C++: Pythia8::ResonanceWidths::width(int, double, int, bool, bool, int, int) --> double", pybind11::arg("idSgn"), pybind11::arg("mHatIn"), pybind11::arg("idInFlavIn"), pybind11::arg("openOnly"), pybind11::arg("setBR"), pybind11::arg("idOutFlav1"), pybind11::arg("idOutFlav2"));
		cl.def("widthOpen", [](Pythia8::ResonanceWidths &o, int const & a0, double const & a1) -> double { return o.widthOpen(a0, a1); }, "", pybind11::arg("idSgn"), pybind11::arg("mHatIn"));
		cl.def("widthOpen", (double (Pythia8::ResonanceWidths::*)(int, double, int)) &Pythia8::ResonanceWidths::widthOpen, "C++: Pythia8::ResonanceWidths::widthOpen(int, double, int) --> double", pybind11::arg("idSgn"), pybind11::arg("mHatIn"), pybind11::arg("idIn"));
		cl.def("widthStore", [](Pythia8::ResonanceWidths &o, int const & a0, double const & a1) -> double { return o.widthStore(a0, a1); }, "", pybind11::arg("idSgn"), pybind11::arg("mHatIn"));
		cl.def("widthStore", (double (Pythia8::ResonanceWidths::*)(int, double, int)) &Pythia8::ResonanceWidths::widthStore, "C++: Pythia8::ResonanceWidths::widthStore(int, double, int) --> double", pybind11::arg("idSgn"), pybind11::arg("mHatIn"), pybind11::arg("idIn"));
		cl.def("openFrac", (double (Pythia8::ResonanceWidths::*)(int)) &Pythia8::ResonanceWidths::openFrac, "C++: Pythia8::ResonanceWidths::openFrac(int) --> double", pybind11::arg("idSgn"));
		cl.def("widthRescaleFactor", (double (Pythia8::ResonanceWidths::*)()) &Pythia8::ResonanceWidths::widthRescaleFactor, "C++: Pythia8::ResonanceWidths::widthRescaleFactor() --> double");
		cl.def("widthChan", (double (Pythia8::ResonanceWidths::*)(double, int, int)) &Pythia8::ResonanceWidths::widthChan, "C++: Pythia8::ResonanceWidths::widthChan(double, int, int) --> double", pybind11::arg("mHatIn"), pybind11::arg("idOutFlav1"), pybind11::arg("idOutFlav2"));
		cl.def("initConstants", (void (Pythia8::ResonanceWidths::*)()) &Pythia8::ResonanceWidths::initConstants, "C++: Pythia8::ResonanceWidths::initConstants() --> void");
		cl.def("initBSM", (bool (Pythia8::ResonanceWidths::*)()) &Pythia8::ResonanceWidths::initBSM, "C++: Pythia8::ResonanceWidths::initBSM() --> bool");
		cl.def("allowCalc", (bool (Pythia8::ResonanceWidths::*)()) &Pythia8::ResonanceWidths::allowCalc, "C++: Pythia8::ResonanceWidths::allowCalc() --> bool");
		cl.def("calcPreFac", [](Pythia8::ResonanceWidths &o) -> void { return o.calcPreFac(); }, "");
		cl.def("calcPreFac", (void (Pythia8::ResonanceWidths::*)(bool)) &Pythia8::ResonanceWidths::calcPreFac, "C++: Pythia8::ResonanceWidths::calcPreFac(bool) --> void", pybind11::arg(""));
		cl.def("calcWidth", [](Pythia8::ResonanceWidths &o) -> void { return o.calcWidth(); }, "");
		cl.def("calcWidth", (void (Pythia8::ResonanceWidths::*)(bool)) &Pythia8::ResonanceWidths::calcWidth, "C++: Pythia8::ResonanceWidths::calcWidth(bool) --> void", pybind11::arg(""));
		cl.def("numInt1BW", [](Pythia8::ResonanceWidths &o, double const & a0, double const & a1, double const & a2, double const & a3, double const & a4) -> double { return o.numInt1BW(a0, a1, a2, a3, a4); }, "", pybind11::arg("mHatIn"), pybind11::arg("m1"), pybind11::arg("Gamma1"), pybind11::arg("mMin1"), pybind11::arg("m2"));
		cl.def("numInt1BW", (double (Pythia8::ResonanceWidths::*)(double, double, double, double, double, int)) &Pythia8::ResonanceWidths::numInt1BW, "C++: Pythia8::ResonanceWidths::numInt1BW(double, double, double, double, double, int) --> double", pybind11::arg("mHatIn"), pybind11::arg("m1"), pybind11::arg("Gamma1"), pybind11::arg("mMin1"), pybind11::arg("m2"), pybind11::arg("psMode"));
		cl.def("numInt2BW", [](Pythia8::ResonanceWidths &o, double const & a0, double const & a1, double const & a2, double const & a3, double const & a4, double const & a5, double const & a6) -> double { return o.numInt2BW(a0, a1, a2, a3, a4, a5, a6); }, "", pybind11::arg("mHatIn"), pybind11::arg("m1"), pybind11::arg("Gamma1"), pybind11::arg("mMin1"), pybind11::arg("m2"), pybind11::arg("Gamma2"), pybind11::arg("mMin2"));
		cl.def("numInt2BW", (double (Pythia8::ResonanceWidths::*)(double, double, double, double, double, double, double, int)) &Pythia8::ResonanceWidths::numInt2BW, "C++: Pythia8::ResonanceWidths::numInt2BW(double, double, double, double, double, double, double, int) --> double", pybind11::arg("mHatIn"), pybind11::arg("m1"), pybind11::arg("Gamma1"), pybind11::arg("mMin1"), pybind11::arg("m2"), pybind11::arg("Gamma2"), pybind11::arg("mMin2"), pybind11::arg("psMode"));
		cl.def("assign", (class Pythia8::ResonanceWidths & (Pythia8::ResonanceWidths::*)(const class Pythia8::ResonanceWidths &)) &Pythia8::ResonanceWidths::operator=, "C++: Pythia8::ResonanceWidths::operator=(const class Pythia8::ResonanceWidths &) --> class Pythia8::ResonanceWidths &", pybind11::return_value_policy::reference, pybind11::arg(""));
	}
	{ // Pythia8::ResonanceGeneric file:Pythia8/ResonanceWidths.h line:149
		pybind11::class_<Pythia8::ResonanceGeneric, std::shared_ptr<Pythia8::ResonanceGeneric>, PyCallBack_Pythia8_ResonanceGeneric, Pythia8::ResonanceWidths> cl(M("Pythia8"), "ResonanceGeneric", "");
		pybind11::handle cl_type = cl;

		cl.def( pybind11::init<int>(), pybind11::arg("idResIn") );

		cl.def("allowCalc", (bool (Pythia8::ResonanceGeneric::*)()) &Pythia8::ResonanceGeneric::allowCalc, "C++: Pythia8::ResonanceGeneric::allowCalc() --> bool");
		cl.def("assign", (class Pythia8::ResonanceGeneric & (Pythia8::ResonanceGeneric::*)(const class Pythia8::ResonanceGeneric &)) &Pythia8::ResonanceGeneric::operator=, "C++: Pythia8::ResonanceGeneric::operator=(const class Pythia8::ResonanceGeneric &) --> class Pythia8::ResonanceGeneric &", pybind11::return_value_policy::reference, pybind11::arg(""));
	}
	{ // Pythia8::ResonanceGmZ file:Pythia8/ResonanceWidths.h line:165
		pybind11::class_<Pythia8::ResonanceGmZ, std::shared_ptr<Pythia8::ResonanceGmZ>, PyCallBack_Pythia8_ResonanceGmZ, Pythia8::ResonanceWidths> cl(M("Pythia8"), "ResonanceGmZ", "");
		pybind11::handle cl_type = cl;

		cl.def( pybind11::init<int>(), pybind11::arg("idResIn") );

		cl.def("assign", (class Pythia8::ResonanceGmZ & (Pythia8::ResonanceGmZ::*)(const class Pythia8::ResonanceGmZ &)) &Pythia8::ResonanceGmZ::operator=, "C++: Pythia8::ResonanceGmZ::operator=(const class Pythia8::ResonanceGmZ &) --> class Pythia8::ResonanceGmZ &", pybind11::return_value_policy::reference, pybind11::arg(""));
	}
	{ // Pythia8::ResonanceW file:Pythia8/ResonanceWidths.h line:194
		pybind11::class_<Pythia8::ResonanceW, std::shared_ptr<Pythia8::ResonanceW>, PyCallBack_Pythia8_ResonanceW, Pythia8::ResonanceWidths> cl(M("Pythia8"), "ResonanceW", "");
		pybind11::handle cl_type = cl;

		cl.def( pybind11::init<int>(), pybind11::arg("idResIn") );

		cl.def("assign", (class Pythia8::ResonanceW & (Pythia8::ResonanceW::*)(const class Pythia8::ResonanceW &)) &Pythia8::ResonanceW::operator=, "C++: Pythia8::ResonanceW::operator=(const class Pythia8::ResonanceW &) --> class Pythia8::ResonanceW &", pybind11::return_value_policy::reference, pybind11::arg(""));
	}
	{ // Pythia8::ResonanceTop file:Pythia8/ResonanceWidths.h line:221
		pybind11::class_<Pythia8::ResonanceTop, std::shared_ptr<Pythia8::ResonanceTop>, PyCallBack_Pythia8_ResonanceTop, Pythia8::ResonanceWidths> cl(M("Pythia8"), "ResonanceTop", "");
		pybind11::handle cl_type = cl;

		cl.def( pybind11::init<int>(), pybind11::arg("idResIn") );

		cl.def("assign", (class Pythia8::ResonanceTop & (Pythia8::ResonanceTop::*)(const class Pythia8::ResonanceTop &)) &Pythia8::ResonanceTop::operator=, "C++: Pythia8::ResonanceTop::operator=(const class Pythia8::ResonanceTop &) --> class Pythia8::ResonanceTop &", pybind11::return_value_policy::reference, pybind11::arg(""));
	}
	{ // Pythia8::ResonanceFour file:Pythia8/ResonanceWidths.h line:249
		pybind11::class_<Pythia8::ResonanceFour, std::shared_ptr<Pythia8::ResonanceFour>, PyCallBack_Pythia8_ResonanceFour, Pythia8::ResonanceWidths> cl(M("Pythia8"), "ResonanceFour", "");
		pybind11::handle cl_type = cl;

		cl.def( pybind11::init<int>(), pybind11::arg("idResIn") );

		cl.def("assign", (class Pythia8::ResonanceFour & (Pythia8::ResonanceFour::*)(const class Pythia8::ResonanceFour &)) &Pythia8::ResonanceFour::operator=, "C++: Pythia8::ResonanceFour::operator=(const class Pythia8::ResonanceFour &) --> class Pythia8::ResonanceFour &", pybind11::return_value_policy::reference, pybind11::arg(""));
	}
	{ // Pythia8::ResonanceH file:Pythia8/ResonanceWidths.h line:277
		pybind11::class_<Pythia8::ResonanceH, std::shared_ptr<Pythia8::ResonanceH>, PyCallBack_Pythia8_ResonanceH, Pythia8::ResonanceWidths> cl(M("Pythia8"), "ResonanceH", "");
		pybind11::handle cl_type = cl;

		cl.def( pybind11::init<int, int>(), pybind11::arg("higgsTypeIn"), pybind11::arg("idResIn") );

		cl.def("assign", (class Pythia8::ResonanceH & (Pythia8::ResonanceH::*)(const class Pythia8::ResonanceH &)) &Pythia8::ResonanceH::operator=, "C++: Pythia8::ResonanceH::operator=(const class Pythia8::ResonanceH &) --> class Pythia8::ResonanceH &", pybind11::return_value_policy::reference, pybind11::arg(""));
	}
	{ // Pythia8::ResonanceHchg file:Pythia8/ResonanceWidths.h line:330
		pybind11::class_<Pythia8::ResonanceHchg, std::shared_ptr<Pythia8::ResonanceHchg>, PyCallBack_Pythia8_ResonanceHchg, Pythia8::ResonanceWidths> cl(M("Pythia8"), "ResonanceHchg", "");
		pybind11::handle cl_type = cl;

		cl.def( pybind11::init<int>(), pybind11::arg("idResIn") );

		cl.def("assign", (class Pythia8::ResonanceHchg & (Pythia8::ResonanceHchg::*)(const class Pythia8::ResonanceHchg &)) &Pythia8::ResonanceHchg::operator=, "C++: Pythia8::ResonanceHchg::operator=(const class Pythia8::ResonanceHchg &) --> class Pythia8::ResonanceHchg &", pybind11::return_value_policy::reference, pybind11::arg(""));
	}
	{ // Pythia8::ResonanceZprime file:Pythia8/ResonanceWidths.h line:359
		pybind11::class_<Pythia8::ResonanceZprime, std::shared_ptr<Pythia8::ResonanceZprime>, PyCallBack_Pythia8_ResonanceZprime, Pythia8::ResonanceWidths> cl(M("Pythia8"), "ResonanceZprime", "");
		pybind11::handle cl_type = cl;

		cl.def( pybind11::init<int>(), pybind11::arg("idResIn") );

		cl.def("assign", (class Pythia8::ResonanceZprime & (Pythia8::ResonanceZprime::*)(const class Pythia8::ResonanceZprime &)) &Pythia8::ResonanceZprime::operator=, "C++: Pythia8::ResonanceZprime::operator=(const class Pythia8::ResonanceZprime &) --> class Pythia8::ResonanceZprime &", pybind11::return_value_policy::reference, pybind11::arg(""));
	}
	{ // Pythia8::ResonanceWprime file:Pythia8/ResonanceWidths.h line:392
		pybind11::class_<Pythia8::ResonanceWprime, std::shared_ptr<Pythia8::ResonanceWprime>, PyCallBack_Pythia8_ResonanceWprime, Pythia8::ResonanceWidths> cl(M("Pythia8"), "ResonanceWprime", "");
		pybind11::handle cl_type = cl;

		cl.def( pybind11::init<int>(), pybind11::arg("idResIn") );

		cl.def("assign", (class Pythia8::ResonanceWprime & (Pythia8::ResonanceWprime::*)(const class Pythia8::ResonanceWprime &)) &Pythia8::ResonanceWprime::operator=, "C++: Pythia8::ResonanceWprime::operator=(const class Pythia8::ResonanceWprime &) --> class Pythia8::ResonanceWprime &", pybind11::return_value_policy::reference, pybind11::arg(""));
	}
}
